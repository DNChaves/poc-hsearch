package br.com.dnchaves.pochsearch.controller;

import br.com.dnchaves.pochsearch.entity.Sorteio;
import br.com.dnchaves.pochsearch.repository.SorteioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;

@RestController
@RequestMapping("/sorteios")
public class SorteioController {

    @Autowired
    private SorteioRepository repository;

    @GetMapping(path="/{id}")
    public Sorteio findById(@PathVariable Long id) {
        return repository.findById(id).orElseThrow(EntityNotFoundException::new);
    }

    @PostMapping(path = "/find")
    public Page<Sorteio> find(@RequestBody Sorteio sorteio, Pageable pageable) {
        return repository.findAll(Example.of(sorteio), pageable);
    }

    @GetMapping(path="/findAll")
    public Page<Sorteio> findAll(Pageable pageable) {
        return repository.findAll(pageable);
    }
}
