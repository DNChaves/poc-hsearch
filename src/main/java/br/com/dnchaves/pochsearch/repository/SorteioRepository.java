package br.com.dnchaves.pochsearch.repository;

import br.com.dnchaves.pochsearch.entity.Sorteio;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface SorteioRepository extends JpaRepository<Sorteio, Long> {

    Optional<Sorteio> findById(Long id);

    List<Sorteio> findByDataSorteio(LocalDate data);
}
