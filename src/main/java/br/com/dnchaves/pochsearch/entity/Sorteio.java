package br.com.dnchaves.pochsearch.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "sorteio")
public class Sorteio implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Temporal(TemporalType.DATE)
    private Date dataSorteio;

    private String numero01;
    private String numero02;
    private String numero03;
    private String numero04;
    private String numero05;
    private String numero06;

    private Integer numeroVencedores;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDataSorteio() {
        return dataSorteio;
    }

    public void setDataSorteio(Date dataSorteio) {
        this.dataSorteio = dataSorteio;
    }

    public String getNumero01() {
        return numero01;
    }

    public void setNumero01(String numero01) {
        this.numero01 = numero01;
    }

    public String getNumero02() {
        return numero02;
    }

    public void setNumero02(String numero02) {
        this.numero02 = numero02;
    }

    public String getNumero03() {
        return numero03;
    }

    public void setNumero03(String numero03) {
        this.numero03 = numero03;
    }

    public String getNumero04() {
        return numero04;
    }

    public void setNumero04(String numero04) {
        this.numero04 = numero04;
    }

    public String getNumero05() {
        return numero05;
    }

    public void setNumero05(String numero05) {
        this.numero05 = numero05;
    }

    public String getNumero06() {
        return numero06;
    }

    public void setNumero06(String numero06) {
        this.numero06 = numero06;
    }

    public Integer getNumeroVencedores() {
        return numeroVencedores;
    }

    public void setNumeroVencedores(Integer numeroVencedores) {
        this.numeroVencedores = numeroVencedores;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Sorteio sorteio = (Sorteio) o;
        return Objects.equals(id, sorteio.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
