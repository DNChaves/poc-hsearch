package br.com.dnchaves.pochsearch.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EntityScan(basePackages = {"br.com.dnchaves.pochsearch.entity"})
@EnableJpaRepositories(basePackages = {"br.com.dnchaves.pochsearch.repository"})
@ComponentScan(basePackages = {"br.com.dnchaves.pochsearch.controller"})
public class HSearchApplication {

    public static void main(String[] args) {
        SpringApplication.run(HSearchApplication.class, args);
    }
}
